'''
I=1 J=7
I=1 J=6
I=1 J=5
I=3 J=9
I=3 J=8
I=3 J=7
...
I=9 J=15
I=9 J=14
I=9 J=13
'''
for i in range(1, 10, 2):
    for j in range(i+6, i+3, -1):
        print(f'I={i} J={j}')

