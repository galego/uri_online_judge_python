'''
A fórmula para calcular a área de uma circunferência é: area = π . raio2.
Considerando para este problema que π = 3.14159:

- Efetue o cálculo da área, elevando o valor de raio ao quadrado e
multiplicando por π.
Saída com 4 casas decimais.

Entrada       Saída
2.00          A=12.5664
'''

import math

# Valor do raio
raio = float(input())

# Cálculo da área
area = math.pi * raio **2
print('A={area:.4f}m²'.format(area=area))

