'''
Leia 5 valores Inteiros. A seguir mostre quantos valores digitados foram
pares, quantos valores digitados foram ímpares, quantos valores digitados
foram positivos e quantos valores digitados foram negativos.

Entrada         Saída
-5              3 valor(es) par(es)
0               2 valor(es) impar(es)
-3              1 valor(es) positivo(s)
-4              3 valor(es) negativo(s)
12
'''
par = 0
impar = 0
pos = 0
neg = 0

for i in range(5):
    x = int(input())
    if x % 2 == 0:
        par += 1
    else:
        impar += 1
    if x == 0:
        pass
    elif x > 0:
        pos += 1
    else:
        neg += 1

print('''{} valor(es) par(es)
{} valor(es) impar(es)
{} valor(es) positivo(s)
{} valor(es) negativo(s)'''.format(par, impar, pos, neg))

