'''
Faça um programa que leia 6 valores. Estes valores serão somente negativos
ou positivos (desconsidere os valores nulos). A seguir, mostre a quantidade
de valores positivos digitados.

Entrada         Saída
7               4 valores positivos
-5
6
-3.4
4.6
12
'''

positivos = 0
for i in range(6):
    x = float(input())  # f'{i+1}º número: '
    if x > 0:
        positivos += 1

print(f'{positivos} valores positivos')

