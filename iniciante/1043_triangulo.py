'''
Leia 3 valores reais (A, B e C) e verifique se eles formam ou não um
triângulo. Em caso positivo, calcule o perímetro do triângulo e apresente a
mensagem:
Perimetro = XX.X

Em caso negativo, calcule a área do trapézio que tem A e B como base e C
como altura, mostrando a mensagem
Area = XX.X

Entrada             Saída
6.0 4.0 2.0         Area = 10.0
6.0 4.0 2.1         Perimetro = 12.1
'''

a, b, c = input().split()  # Valores dos lados do possível triângulo
a = float(a)
b = float(b)
c = float(c)

if (abs(b-c) < a < b+c) or (abs(a-c) < b < a+c) or (abs(a-b) < c < a+b):
    perimetro = a+b+c
    print('Perimetro = {:3.1f}'.format(perimetro))
else:
    area = (c*(a+b))/2
    print('Area = {:3.1f}'.format(area))

