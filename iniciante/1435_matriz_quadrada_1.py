'''
Escreva um algoritmo que leia um inteiro N (0 ≤ N ≤ 100), correspondente a
ordem de uma matriz M de inteiros, e construa a matriz de acordo com o
exemplo abaixo.

Entrada
A entrada consiste de vários inteiros, um valor por linha, correspondentes
as ordens das matrizes a serem construídas. O final da entrada é marcado
por um valor de ordem igual a zero (0).

Saída
Para cada inteiro da entrada imprima a matriz correspondente, de acordo com
o exemplo. Os valores das matrizes devem ser formatados em um campo de
tamanho 3 justificados à direita e separados por espaço. Após o último
caractere de cada linha da matriz não deve haver espaços em branco. Após a
impressão de cada matriz deve ser deixada uma linha em branco.

Entrada         Saída
1               1

2               1 1
                1 1

3               1 1 1
                1 2 1
                1 1 1

4               1 1 1 1
                1 2 2 1
                1 2 2 1
                1 1 1 1
0
######################################
Explicando minha lógica

A primeira linha da matriz é sempre composta por valores 1
1 1 1 1 1 1

'''
while True:
    ordem = int(input())    # Valor da ordem da matriz. 2 => 2x2, 7 => 7x7
    if ordem == 0:          # Quando 'ordem' for 0, finalizar
        break

    matriz = []             # Inicialmente a matriz fica vazia
    base = [0 for i in range(ordem)]

    i = 0
    j = ordem
    while i < j:
        for c in range(i, j):
            base[c] += 1
        linha = base[:]
        matriz.append(linha)
        i += 1
        j -= 1

    exibir = '{:>3} ' * ordem
    exibir = exibir.strip()

    if ordem % 2 == 0:
        matriz_completa = matriz + matriz[::-1]
    else:
        matriz_completa = matriz + matriz[-2::-1]

    for l in matriz_completa:
        print(exibir.format(*l))
    print()

