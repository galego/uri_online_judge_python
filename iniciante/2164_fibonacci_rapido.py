'''
A fórmula de Binet é uma forma de calcular números de Fibonacci.

Sua tarefa é, dado um natural n, calcular o valor de Fibonacci(n) usando a fórmula
acima.

fib(n) = ( ((1+v5) / 2)**n - ((1-v5)/2)**n ) / v5

Entrada
A entrada é um número natural n (0 < n ≤ 50).

Saída
A saída é o valor de Fibonacci(n) com 1 casa decimal utilizando a fórmula de Binet
dada.

Input
1

Output
1.0
'''
import math


n = int(input())

fib = ( ( (1+math.sqrt(5)) / 2 )**n - ( (1-math.sqrt(5)) / 2 )**n ) / math.sqrt(5)
print('{:.1f}'.format(fib))

