'''
Na verdade, o enunciado pede 1 valor para A.
O valor de N tem que ser maior que 0, e podem ser passados vários números
ao mesmo tempo. Logo, uma lista, para pegar o primeiro positivo.

Tem uma explicação bacana nesse post:
https://www.urionlinejudge.com.br/judge/pt/questions/view/1149/1491
'''
lista = list(map(int, input().split()))
a = lista[0]   # Definindo a variável A.
for i in lista[1:]:  # Encontrar o valor positivo para a variável N
    if i > 0:
        n = i
        break

# Agora vamos fazer a soma que se pede no enunciado
soma = 0
for i in range(n):
    soma += a
    a += 1

print(soma)

