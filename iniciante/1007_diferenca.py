'''
Leia quatro valores inteiros A, B, C e D. A seguir, calcule e mostre a
diferença do produto de A e B pelo produto de C e D segundo a fórmula:
DIFERENCA = (A * B - C * D).

Entrada        Saída
5
6
7
8              DIFERENÇA = -26
'''
# Valores A, B, C e D
a = int(input())
b = int(input())
c = int(input())
d = int(input())

diferenca = (a*b - c*d)

print(f'DIFERENÇA = {diferenca}')

