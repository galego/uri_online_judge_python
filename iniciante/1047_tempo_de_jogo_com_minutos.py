'''
Leia a hora inicial, minuto inicial, hora final e minuto final de um jogo.
A seguir calcule a duração do jogo.

Obs: O jogo tem duração mínima de um (1) minuto e duração máxima de 24 horas
Mostre a seguinte mensagem: “O JOGO DUROU XXX HORA(S) E YYY MINUTO(S)” .

Entrada         Saída
7 8 9 10        O JOGO DUROU 2 HORA(S) E 2 MINUTO(S)
7 7 7 7         O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)
7 10 8 9        O JOGO DUROU 0 HORA(S) E 59 MINUTO(S)
'''

def calcular_horas(inicial, final):
    if inicial >= final:
        total = 24 - inicial + final 
    else:
        total = final - inicial
    return total

def calcular_minutos(inicial, final):
    diminuir_hora = False
    if inicial > final:
        diminuir_hora = True
        total = 60 - inicial + final
    else:
        total = final - inicial
    return total, diminuir_hora

# start|inicio - hora e minuto inicial
# end|fim - hora e minuto final
start, inicio, end, fim = input().split()  # Duração do jogo igual exemplo
start = int(start)
inicio = int(inicio)
end = int(end)
fim = int(fim)

horas = calcular_horas(start, end)
minutos, diminuir = calcular_minutos(inicio, fim)

if start == end and inicio < fim:
    horas -= 24
elif diminuir:
    horas -= 1

print('O JOGO DUROU {} HORA(S) E {} MINUTO(S)'.format(horas, minutos))

