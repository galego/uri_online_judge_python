'''
Leia um caractere maiúsculo, que indica uma operação que deve ser realizada
e uma matriz M[12][12]. Em seguida, calcule e mostre a soma ou a média
considerando somente aqueles elementos que estão acima da diagonal
principal da matriz, conforme ilustrado abaixo (área verde).

Entrada
A primeira linha de entrada contem um único caractere Maiúsculo O ('S' ou
'M'), indicando a operação (Soma ou Média) que deverá ser realizada com os
elementos da matriz. Seguem os 144 valores de ponto flutuante que compõem a
matriz.

Saída
Imprima o resultado solicitado (a soma ou média), com 1 casa após o ponto
decimal.

Entrada     Saída
S           12.6
1.0
0.0
-3.5
2.5
4.1
...
'''
op = input().upper()

soma = 0
matriz = []
itens = 0                   # Itens acima da diagonal superior
for l in range(12):
    linha = []
    for c in range(12):
        x = float(input())
        if l < c:
            itens += 1
            soma += x
        linha.append(x)
    matriz.append(linha)

if op == 'S':
    print('{:.1f}'.format(soma))
elif op == 'M':
    media = soma / itens
    print('{:.1f}'.format(media))

