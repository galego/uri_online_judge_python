'''
Leia um caractere maiúsculo, que indica uma operação que deve ser realizada
e uma matriz M[12][12]. Em seguida, calcule e mostre a soma ou a média
considerando somente aqueles elementos que estão na área esquerda da matriz,
conforme ilustrado abaixo (área verde).

Entrada
A primeira linha de entrada contem um único caractere Maiúsculo O ('S' ou
'M'), indicando a operação (Soma ou Média) que deverá ser realizada com os
elementos da matriz. Seguem os 144 valores de ponto flutuante que compõem a
matriz.

Saída
Imprima o resultado solicitado (a soma ou média), com 1 casa após o ponto
decimal.

Entrada     Saída
S           111.4
1.0
330.0
-3.5
2.5
4.1
...
'''
from decimal import Decimal

def criar_matriz():
    matrix = []
    for l in range(12):
        line = []
        for c in range(12):
            x = Decimal(input())
            line.append(x)
        matrix.append(line)
    return matrix

def rotacionar_matriz(matrix):
    # 'Rotaciona' a matriz para o lado direito
    m = []
    for coluna in range(12):
        l = []
        for linha in range(12):
            x = matrix[linha][coluna]
            l.append(x)
        m.append(l)
    return m

def extrair_dados(matrix):
    soma = 0
    itens = 0
    i = 1
    j = 11
    for linha in matrix:
        l = linha[i:j]
        soma = soma + sum(l)
        itens = itens + len(l)
        i += 1
        j -= 1
        if i >= j:
            break
    return soma, itens

op = input().upper()

# Criar matriz
matriz = criar_matriz()
matriz_aux = rotacionar_matriz(matriz)
soma, itens = extrair_dados(matriz_aux)

if op == 'S':
    print('{:.1f}'.format(soma))
elif op == 'M':
    media = soma/itens
    print('{:.1f}'.format(media))

