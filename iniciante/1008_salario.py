'''
Escreva um programa que leia o número de um funcionário, seu número de horas
trabalhadas, o valor que recebe por hora e calcula o salário desse
funcionário. A seguir, mostre o número e o salário do funcionário, com duas
casas decimais.

Entrada        Saída
25
100            NUMBER = 25
5.50           SALARY = U$ 550.00
'''
num_fun = int(input())  # Número do funcionário
horas = int(input())  # Quantidade de horas trabalhadas
valor_hora = float(input())  # Valor da hora trabalhada

salario = horas * valor_hora

print('NUMBER: %i' %num_fun)
print(f'SALARY: U$ {salario:.2f}')

