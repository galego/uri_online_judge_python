# Meu código (1164_numero_perfeito.py) funciona perfeitamente
# mas não foi aceito pelo site urionlinejudge.
# Encontrei este com uma lógica que difere apenas no modo como
# o laço 'for' é executado.
# Enfim, esse aqui foi aceito.
qte = int(input())

for q in range(qte):
    n = int(input())
    soma = 0
    for i in range(1,n):
        if(n%i == 0):
            soma += i
    if(soma == n):
        print("%d eh perfeito" %n)
    else:
        print("%d nao eh perfeito" %n)

