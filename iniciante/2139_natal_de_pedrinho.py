'''
Pedrinho é um garoto que adora festas em família, principalmente o Natal,
quando ganha presente dos pais e dos avós. Esse ano, seu pai lhe prometeu um
PS4, mas somente se Pedrinho conseguir resolver alguns desafios ao longo do
ano, sendo um deles, escrever um programa que calcule quantos dias faltam
para o Natal.
Entretanto, Pedrinho tem somente 9 anos e não tem noção alguma de
programação, mas sabe que você, primo dele, mexe com "coisas de computador",
e dessa forma, pediu para você escrever o programa para ele. Não somente
isso, mas prometeu que deixaria você jogar todo final de semana e por quanto
tempo quiser!

Entrada
A entrada é composta por vários casos de teste. Cada linha contém o mês e o
dia do ano de 2016 (ano bissexto). A entrada termina com fim de arquivo.

Saída
Se for Natal, imprima "E natal!"; se faltar somente um dia, imprima "E
vespera de natal!"; se já passou, imprima "Ja passou!". Caso contrário,
imprima "Faltam X dias para o natal!", sendo X o número de dias que faltam
para o Natal.

Input
12 24
11 24
12 29
1 5
12 25

Output
E vespera de natal!
Faltam 31 dias para o natal!
Ja passou!
Faltam 355 dias para o natal!
E natal!
'''
# meses e quantidade de dias, por numeração 1=janeiro ...
meses = {1: 31, 2: 29, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30,
        10: 31, 11: 30, 12: 31,}
while 1:
    try:
        mes, dia = map(int, input().split())
    except EOFError:
        break
    soma = 0
    if mes == 12:
        if dia == 24:
            print('E vespera de natal!')
        elif dia > 25:
            print('Ja passou!')
        elif dia < 25:
            print('Faltam {} dia para o natal!'.format(25-dia))
        else:
            print('E natal!')
    else:
        for i in range(mes, 12):
            soma += meses[i]  # Somar todos os dias até o natal
        soma -= dia  # Tirar a quantidade de dias já passados nesse mês
        soma += 25   # Adicionar os 25 dias para chegada do natal
        print('Faltam {} dias para o natal!'.format(soma))

