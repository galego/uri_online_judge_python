'''
Leia 1 valor inteiro N, que representa o número de casos de teste que vem a
seguir. Cada caso de teste consiste de 3 valores reais, cada um deles com
uma casa decimal. Apresente a média ponderada para cada um destes conjuntos
de 3 valores, sendo que o primeiro valor tem peso 2, o segundo valor tem
peso 3 e o terceiro valor tem peso 5.

Entrada         Saída
3               5.7
6.5 4.3 6.2     6.3
5.1 4.2 8.1     9.3
8.0 9.0 10.0
'''
n = int(input())

for i in range(n):
    valores = input().split()
    valores = list(map(float, valores))
    a, b, c = valores

    mp = (a*2 + b*3 + c*5) / (2+3+5)
    print("{:.1f}".format(mp))

