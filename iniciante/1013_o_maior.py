'''
Faça um programa que leia três valores e apresente o maior dos três valores
lidos seguido da mensagem “eh o maior”. Utilize a fórmula:
maiorAB = (a+b+abs(a-b))/2

Obs.: a fórmula apenas calcula o maior entre os dois primeiros (a e b).
Um segundo passo, portanto é necessário para chegar no resultado esperado.

O arquivo de entrada contém três valores inteiros.

Entrada              Saída
7 14 106             106 eh o maior
217 14 6             217 eh o maior
'''
# Input dos valores A, B e C
a, b, c = input().split()
a = int(a)
b = int(b)
c = int(c)

# Cálculo do maior entre 2 números aplicando a fórmula
x = (a+b+abs(a-b)) / 2
# Mais uma vez para o número que faltou comparar
maiorAB = (x+c+abs(x-c)) / 2
print(f'{int(maiorAB)} eh o maior')

