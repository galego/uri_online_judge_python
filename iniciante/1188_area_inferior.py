'''
Leia um caractere maiúsculo, que indica uma operação que deve ser realizada
e uma matriz M[12][12]. Em seguida, calcule e mostre a soma ou a média
considerando somente aqueles elementos que estão na área inferior da matriz,
conforme ilustrado abaixo (área verde).

Entrada
A primeira linha de entrada contem um único caractere Maiúsculo O ('S' ou
'M'), indicando a operação (Soma ou Média) que deverá ser realizada com os
elementos da matriz. Seguem os 144 valores de ponto flutuante de dupla
precisão (double) que compõem a matriz.

Saída
Imprima o resultado solicitado (a soma ou média), com 1 casa após o ponto
decimal.

Entrada         Saída
S               112.4
1.0
330.0
-3.5
2.5
4.1
...
'''
from decimal import Decimal
op = input().upper()

matriz = []
for l in range(12):
    linha = []
    for c in range(12):
        x = Decimal(input())
        linha.append(x)
    matriz.append(linha)

soma = 0
itens = 0
i = 1
j = 11
for l in matriz[::-1]:
    lista = l[i:j]
    soma = soma + sum(lista)
    itens = itens + len(lista)
    i += 1
    j -= 1
    if i >= j:
        break

if op == 'S':
    print('{:.1f}'.format(soma))
elif op == 'M':
    media = soma/itens
    print('{:.1f}'.format(media))

