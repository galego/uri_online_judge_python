'''
A empresa ABC resolveu conceder um aumento de salários a seus funcionários
de acordo com a tabela abaixo:

    Salário         Percentual de Reajuste
  0 - 400.00            15%
 400.01 - 800.00        12%
 800.01 - 1200.00       10%
1200.01 - 2000.00        7%
Acima de 2000.00         4%

Leia o salário do funcionário e calcule e mostre o novo salário, bem como o
valor de reajuste ganho e o índice reajustado, em percentual.

Entrada         Saída
400.00          Novo salario: 460.00
                Reajuste ganho: 60.00
                Em percentual: 15 %

800.01          Novo salario: 880.01
                Reajuste ganho: 80.00
                Em percentual: 10 %

2000.00         Novo salario: 2140.00
                Reajuste ganho: 140.00
                Em percentual: 7 %
'''
salario = float(input())  # Salário atual

if salario <= 400:
    tx = 15
elif salario <= 800:
    tx = 12
elif salario <= 1200:
    tx = 10
elif salario <= 2000:
    tx = 7
else:
    tx = 4

reajuste = (tx/100) * salario
novo_salario = salario + reajuste

print('''Novo salario: {:.2f}
Reajuste ganho: {:.2f}
Em percentual: {} %'''.format(novo_salario, reajuste, tx))

