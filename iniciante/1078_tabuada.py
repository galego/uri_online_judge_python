'''
Leia 1 valor inteiro N (2 < N < 1000). A seguir, mostre a tabuada de N:
1 x N = N      2 x N = 2N        ...       10 x N = 10N

Entrada
A entrada contém um valor inteiro N (2 < N < 1000).
'''
n = int(input())
for i in range(1, 11):
    p = i*n
    print('{} x {} = {}'.format(i, n, p))

