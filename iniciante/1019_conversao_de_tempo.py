'''
Leia um valor inteiro, que é o tempo de duração em segundos de um
determinado evento em uma fábrica, e informe-o expresso no formato
horas:minutos:segundos.

Entrada
O arquivo de entrada contém um valor inteiro N.

Saída
Imprima o tempo lido no arquivo de entrada (segundos), convertido para
horas:minutos:segundos, conforme exemplo fornecido.

556    ==>> 0:9:16
140153 ==>> 38:55:53
'''
tempo = int(input())  # Segundos
ss = tempo % 60
mm = tempo // 60 % 60
hh = tempo // 60 // 60
print('{0}:{1}:{2}'.format(hh, mm, ss))

