'''
Leia 100 valores inteiros. Apresente então o maior valor lido e a posição
dentre os 100 valores lidos.

Entrada     Saída
2           34565
113         4
45
34565
6
...
8
'''
maior = 0
for i in range(100):
    x = int(input())
    if x > maior:
        maior = x
        index = i+1

print(maior)
print(index)

