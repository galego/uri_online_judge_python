'''
Após comprar vários sítios adjacentes na região do oeste catarinense, a
família Estrela construiu uma única estrada que passa por todos os sítios
em sequência. O primeiro sítio da sequência foi batizado de Estrela 1, o
segundo de Estrela 2, e assim por diante. Porém, o irmão que vive em
Estrela 1 acabou enlouquecendo e resolveu fazer uma Jornada nas Estrelas
para roubar carneiros das propriedades de seus irmãos. Mas ele está
definitivamente pirado. Quando passa pelo sítio Estrela i, ele rouba apenas
um carneiro daquele sítio (se o sítio tem algum) e segue ou para
Estrela i + 1 ou para Estrela i - 1, dependendo se o número de carneiros em
Estrela i era, respectivamente, ímpar ou par. Se não existe a Estrela para
a qual ele deseja seguir, ele interrompe sua jornada. O irmão louco começa
sua Jornada em Estrela 1, roubando um carneiro do seu próprio sítio.

Entrada
A primeira linha da entrada consiste de um único inteiro N (1 ≤ N ≤ 106), o
qual representa o número de Estrelas. A segunda linha da entrada consiste
de N inteiros, de modo que o i-ésimo inteiro, Xi (1 ≤ Xi ≤ 106), representa
o número inicial de carneiros em Estrela i.

Saída
Imprima uma linha contendo dois inteiros, de modo que o primeiro represente
o número de Estrelas atacadas pelo irmão louco e o segundo represente o
número total de carneiros não roubados.

Entrada                 Saída
8
1 3 5 7 11 13 17 19     8 68
8
1 3 5 7 11 13 16 19     7 63
'''
stars = int(input())
lambs = list(map(int, input().split()))

attacked = 0  # Quantidade de estrelas atacadas
carneiros = 0  # Quantidade de carneiros roubados
louco = 1  # índice inicial onde o irmão louco se encontra

'''
# Número da estrela, foi atacado?, quantidade de carneiros
estrelas = {}
for i in range(1, len(lambs) + 1):
    estrelas[i] = [False, lambs[i-1]]
'''
# O mesmo código acima, porém feito por dict/list comprehensions
estrelas = {i: [False, lambs[i-1]] for i in range(1, len(lambs) + 1)}

while louco in estrelas.keys():
    # Onde o louco está roubando
    # Se a quantidade de carneiros nessa estrela é maior que zero, a estrela foi
    # atacada. Remove-se 1 carneiro.
    if estrelas[louco][1] > 0:
        estrelas[louco][0] = True
        estrelas[louco][1] -= 1
        carneiros += 1
