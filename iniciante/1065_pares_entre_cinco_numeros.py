'''
Faça um programa que leia 5 valores inteiros. Conte quantos destes valores
digitados são pares e mostre esta informação.

Entrada         Saída
7               3 valores pares
-5
6
-4
12
'''
cont = 0
for i in range(5):
    x = int(input('Valor : '))
    if x % 2 == 0:
        cont += 1

print(f'{cont} valores pares')

