'''
Escreva um algoritmo que leia 2 números e imprima o resultado da divisão do
primeiro pelo segundo. Caso não for possível mostre a mensagem “divisao
impossivel” para os valores em questão.

A entrada contém um número inteiro N. Este N será a quantidade de pares de
valores inteiros (X e Y) que serão lidos em seguida.

Para cada caso mostre o resultado da divisão com um dígito após o ponto
decimal, ou “divisao impossivel” caso não seja possível efetuar o cálculo.

Obs.: Cuide que a divisão entre dois inteiros em algumas linguagens como o
C e C++ gera outro inteiro. Utilize cast :)

Entrada         Saída
3               -1.5
3 -2            divisao impossivel
-8 0            0.0
0 8
'''
v = int(input('Número de divisões: '))
for i in range(v):
    x, y = input().split()
    x = int(x)
    y = int(y)

    try:
        print('{}'.format(x/y))
    except:
        print('divisao impossivel')

