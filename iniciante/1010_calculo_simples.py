'''
Neste problema, deve-se ler o código de uma peça 1, o número de peças 1, o
valor unitário de cada peça 1, o código de uma peça 2, o número de peças 2 e
o valor unitário de cada peça 2. Após, calcule e mostre o valor a ser pago.

O arquivo de entrada contém duas linhas de dados. Em cada linha haverá 3
valores, respectivamente dois inteiros e um valor com 2 casas decimais.

Entrada             Saída
12 1 5.30
16 2 5.10           VALOR A PAGAR: R$ 15.50
'''
# peça 1, quantidade, preço
pc1, qt_pc1, prc1 = input().split()

# peça 2, quantidade, preço
pc2, qt_pc2, prc2 = input().split()

total = int(qt_pc1)*float(prc1) + int(qt_pc2)*float(prc2)
print(f'VALOR A PAGAR: R$ {total:.2f}')

