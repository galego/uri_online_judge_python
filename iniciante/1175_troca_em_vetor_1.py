'''
Faça um programa que leia um vetor N[20]. Troque a seguir, o primeiro
elemento com o último, o segundo elemento com o penúltimo, etc., até trocar
o 10º com o 11º. Mostre o vetor modificado.

Entrada
A entrada contém 20 valores inteiros, positivos ou negativos.

Saída
Para cada posição do vetor N, escreva "N[i] = Y", onde i é a posição do
vetor e Y é o valor armazenado naquela posição.

Entrada     Saída
0           N[0] = 230
-5          N[1] = 63
...         ...
63          N[18] = -5
230         N[19] = 0
'''
vetor = []
for i in range(20):
    x = int(input())
    vetor.append(x)

# Poderia ser:
# vetor[i], vetor[tal_indice] = vetor[tal_indice], vetor[i]
# mas decidi usar uma variável auxiliar
index = len(vetor) - 1
for i in range(10):
    aux = vetor[i]
    vetor[i] = vetor[index]
    vetor[index] = aux
    index -= 1

for i in range(len(vetor)):
    print('N[{}] = {}'.format(i, vetor[i]))

