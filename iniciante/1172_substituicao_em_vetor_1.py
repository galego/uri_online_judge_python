'''
Faça um programa que leia um vetor X[10]. Substitua a seguir, todos os
valores nulos e negativos do vetor X por 1. Em seguida mostre o vetor X.

Entrada
A entrada contém 10 valores inteiros, podendo ser positivos ou negativos.

Saída
Para cada posição do vetor, escreva "X[i] = x", onde i é a posição do vetor
e x é o valor armazenado naquela posição.

Entrada     Saída
0           X[0] = 1
-5          X[1] = 1
63          X[2] = 63
0           X[3] = 1
...         ...
'''
vetor = []
for i in range(10):
    x = int(input())
    if x <= 0:
        vetor.append(1)
    else:
        vetor.append(x)

for i in range(10):
    print('X[{}] = {}'.format(i, vetor[i]))

