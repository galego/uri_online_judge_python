a, b, c = map(int, input().split())

happy = ':)'
saddy = ':('

if a == b:
    if b >= c:
        print(saddy)
    else:
        print(happy)
elif a > b:
    if b <= c:
        print(happy)
    else:
        if a - b > b - c:
            print(happy)
        else:
            print(saddy)
elif a < b:  # Poderia só usar o 'else' aqui
    if b >= c:
        print(saddy)
    else:
        if c - b < b - a:
            print(saddy)
        else:
            print(happy)

