'''
Gilberto é um famoso vendedor de esfirras na região. Porém, apesar de todos
gostarem de suas esfirras, ele só sabe dar o troco com duas notas, ou seja,
nem sempre é possível receber o troco certo. Para facilitar a vida de Gil,
escreva um programa para ele que determine se é possível ou não devolver o
troco exato utilizando duas notas.
As notas disponíveis são: 2, 5, 10, 20, 50 e 100.

Entrada
A entrada deve conter o valor inteiro N da compra realizada pelo cliente e,
em seguida, o valor inteiro M pago pelo cliente (N < M ≤ 104). A entrada
termina com N = M = 0.

Saída
Seu programa deverá imprimir "possible" se for possível devolver o troco
exato ou "impossible" se não for possível.

Input
11 23
500 650
100 600
9948 9963
1 2
2 4
0 0

Output
possible
possible
impossible
possible
impossible
impossible
'''
notas = [100, 50, 20, 10, 5, 2]
while 1:
    total, recebido = map(int, input().split())
    if not total and not recebido:
        break
    
    numero_de_notas = 0
    troco = recebido - total
    for nota in notas:
        v = troco // nota
        if v:
            numero_de_notas += v
        troco = troco % nota

    if numero_de_notas == 2:
        print('possible')
    else:
        print('impossible')

