'''
Leia um caractere maiúsculo, que indica uma operação que deve ser realizada
e uma matriz M[12][12]. Em seguida, calcule e mostre a soma ou a média
considerando somente aqueles elementos que estão na área superior da matriz,
conforme ilustrado abaixo (área verde).

Entrada
A primeira linha de entrada contem um único caractere Maiúsculo O ('S' ou
'M'), indicando a operação (Soma ou Média) que deverá ser realizada com os
elementos da matriz. Seguem 144 valores com ponto flutuante de dupla
precisão que compõem a matriz.

Saída
Imprima o resultado solicitado (a soma ou média), com 1 casa após o ponto
decimal.

Entrada     Saída
S           112.4
1.0
330.0
-3.5
2.5
4.1
...
'''
# O código funciona normalmente, mas no site dá RuntimeError
from decimal import Decimal
op = input().upper()

matriz = []
for l in range(12):
    linha = []
    for c in range(12):
        x = Decimal(input())
        linha.append(x)
    matriz.append(linha)

soma = itens = 0
i = 1
j = len(matriz[0]) - 1  # O site só aceitou quando usei o valor 11

# Para cada linha da matriz, tenho que somar os valores
# Porém, na 1ª linha, não pega o 1 e último elementos
# Na 2 linha, não pega o 1 e o 2, também o último e penúltimo
for l in matriz:  # l => linha da matriz
    linha = l[i:j]
    soma = soma + sum(linha)
    itens = itens + len(linha)
    i += 1
    j -= 1
    if i >= j:
        break

if op == 'S':
    print('{:.1f}'.format(soma))
elif op == 'M':
    media = soma/itens
    print(f'{media:.1f}')

