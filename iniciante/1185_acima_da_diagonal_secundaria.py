'''
Leia um caractere maiúsculo, que indica uma operação que deve ser
realizada e uma matriz M[12][12]. Em seguida, calcule e mostre a soma ou a
média considerando somente aqueles elementos que estão acima da diagonal
secundária da matriz, conforme ilustrado abaixo (área verde).

Entrada
A primeira linha de entrada contem um único caractere Maiúsculo O ('S' ou
'M'), indicando a operação (Soma ou Média) que deverá ser realizada com os
elementos da matriz. Seguem os 144 valores de ponto flutuante que compõem
a matriz.

Saída
Imprima o resultado solicitado (a soma ou média), com 1 casa após o ponto
decimal.

Entrada         Saída
S               12.6
1.0
0.0
-3.5
2.5
4.1
...
'''
op = input().upper()
soma = 0

matriz = []
for l in range(12):
    linha = []
    for c in range(12):
        x = float(input())
        linha.append(x)
    matriz.append(linha)

# Agora vamos somar os itens acima da diagonal secundária
'''
A diagonal secundária não entra na soma.
Da primeira linha serão usados todos os valores, menos o último
Da segunda, todos menos o último e antepenúltimo.
Sempre diminuindo um item a ser contabilizado (somado) na próxima linha.
A última linha não terá itens a serem somados
'''
# Essas 2 próximas variáveis não são úteis no programa, mas fazem parte
# do processo da minha lógica.
tamanho_matriz = len(matriz)    # Tamanho da matriz
tamanho_linha = len(matriz[0])  # Tamanho de cada linha da matriz

cont = tamanho_linha - 1  # Contador para 'remover' itens da linha ao somar
                          # -1 pois o último índice é 11 e não 12
for linha_da_matriz in matriz:
    soma += sum(linha_da_matriz[:cont])         # Soma dos itens
    itens = itens + len(linha_da_matriz[:cont])  # E quantidade de itens somados
    cont -= 1

if op == 'S':
    print('{:.1f}'.format(soma))
elif op == 'M':
    media = soma / itens
    print('{:.1f}'.format(media))

