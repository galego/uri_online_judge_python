'''
Pedrinho está organizando um evento em sua Universidade. O evento deverá ser
no mês de Abril, iniciando e terminando dentro do mês. O problema é que
Pedrinho quer calcular o tempo que o evento vai durar, uma vez que ele sabe
quando inicia e quando termina o evento.

Sabendo que o evento pode durar de poucos segundos a vários dias, você
deverá ajudar Pedrinho a calcular a duração deste evento.

Como entrada, na primeira linha vai haver a descrição “Dia”, seguido de um
espaço e o dia do mês no qual o evento vai começar. Na linha seguinte, será
informado o momento no qual o evento vai iniciar, no formato hh : mm : ss.
Na terceira e quarta linha de entrada haverá outra informação no mesmo
formato das duas primeiras linhas, indicando o término do evento.

Entrada             Saída
Dia 5               3 dia(s)
08 : 12 : 23        22 hora(s)
Dia 9               1 minuto(s)
06 : 13 : 23        0 segundo(s)
'''

# para o site urionlinejudge.com.br, usar:
'''
primeiro_dia = input().split()
primeiro_dia = int(primeiro_dia[1])

pois ele precisa ler: 'Dia 8' <-- a string
'''
primeiro_dia = int(input('Dia '))
hora_inicio = input('Início do evento: ').split(':')
hora_inicio = [int(k) for k in hora_inicio]

ultimo_dia = int(input('Dia '))
hora_fim = input('Fim do evento: ').split(':')
hora_fim = [int(k) for k in hora_fim]

dias = ultimo_dia - primeiro_dia - 1

# horas, minutos, segundos -> iniciais e finais
hi, mi, si = hora_inicio
hf, mf, sf = hora_fim

if si > sf:
    # se os segundos do horário final do evento forem maiores que os do
    # início, precisa-se fazer o ciclo de 60 e adicionar 1 minuto
    secs = (60 - si) + sf
    mi += 1
else:
    secs = sf - si

if mi > mf:
    # mesma lógica do ciclo dos segundos
    mins = (60 - mi) + mf
    hi += 1
else:
    mins = mf - mi

if hf >= hi:
    # Aqui a pequena diferença é: se as horas finais forem maiores,
    # significa que se acumularam 24h (1 dia completo - o mesmo retirado 
    # na definição da variável 'dias' na linha 31 - só vamos devolver)
    horas = hf - hi
    dias += 1
else:
    horas = (24 - hi) + hf

print('''{} dia(s)
{} hora(s)
{} minuto(s)
{} segundo(s)'''.format(dias, horas, mins, secs))

