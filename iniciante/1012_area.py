'''
Escreva um programa que leia três valores com ponto flutuante de dupla
precisão: A, B e C. Em seguida, calcule e mostre:
a) a área do triângulo retângulo que tem A por base e C por altura.
b) a área do círculo de raio C. (pi = 3.14159)
c) a área do trapézio que tem A e B por bases e C por altura.
d) a área do quadrado que tem lado B.
e) a área do retângulo que tem lados A e B.

O arquivo de entrada contém três valores com um dígito após o ponto decimal.

Entrada              Saída
3.0 4.0 5.2          TRIANGULO: 7.800
                     CIRCULO: 84.949
                     TRAPEZIO: 18.200
                     QUADRADO: 16.000
                     RETANGULO: 12.000
'''
import math

# Input dos valores A, B e C
a, b, c = input().split()
a = float(a)
b = float(b)
c = float(c)

# Cálculos das áreas
triangulo_ret = (a * c) / 2    #a base, c altura
area_circulo = math.pi * c**2  #raio c
area_trapezio = ((a+b)*c) / 2  #a, b bases, c altura
area_quadrado = b**2           #b lado
area_retangulo = a * b              #lados a b

print(f'TRIÂNGULO: {triangulo_ret}')
print(f'CÍRCULO: {area_circulo}')
print(f'TRAPÉZIO: {area_trapezio}')
print(f'QUADRADO: {area_quadrado}')
print(f'RETÂNGULO: {area_retangulo}')

