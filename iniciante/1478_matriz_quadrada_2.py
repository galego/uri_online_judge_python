'''
Escreva um algoritmo que leia um inteiro N (0 ≤ N ≤ 100), correspondente a
ordem de uma matriz M de inteiros, e construa a matriz de acordo com o
exemplo abaixo.

Entrada
A entrada consiste de vários inteiros, um valor por linha, correspondentes
as ordens das matrizes a serem construídas. O final da entrada é marcado
por um valor de ordem igual a zero (0).

Saída
Para cada inteiro da entrada imprima a matriz correspondente, de acordo com
o exemplo. (os valores das matrizes devem ser formatados em um campo de
tamanho 3 justificados à direita e separados por espaço. Após o último
caractere de cada linha da matriz não deve haver espaços em branco. Após a
impressão de cada matriz deve ser deixada uma linha em branco.

Entrada     Saída
1           1

2           1 2
            2 1

3           1 2 3
            2 1 2
            3 2 1

4           1 2 3 4
            2 1 2 3
            3 2 1 2
            4 3 2 1

5           1 2 3 4 5
            2 1 2 3 4
            3 2 1 2 3
            4 3 2 1 2
            5 4 3 2 1
0
'''
while True:
    ordem = int(input())
    if ordem == 0:
        break

    matriz = []
    base = [v for v in range(1, ordem + 1)]
    matriz.append(base)
    i = 0
    j = ordem - 1
    linha = base[:]
    for v in range(j):  #range(ordem-1)
        linha = linha[i:]
        ex = linha.pop()
        
        index = linha.index(1) + 1
        while len(linha) < ordem:
            linha.insert(0, base[index])
            index += 1
        matriz.append(linha)
        i += 1

    exibir = '{:>3} ' * ordem
    exibir = exibir.strip()

    for l in matriz:
        print(exibir.format(*l))
    print()

