'''
Leia dois valores inteiros, no caso para variáveis A e B. A seguir, calcule
a soma entre elas e atribua à variável SOMA. A seguir escrever o valor
desta variável.

Entrada       Saída
30
10            SOMA = 40
'''
# Entrada dos valores de A e B
a = int(input())
b = int(input())
soma = a + b
print('SOMA = %i' %soma)

