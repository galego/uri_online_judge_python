'''
Ler um valor N. Calcular e escrever seu respectivo fatorial. Fatorial de
N = N * (N-1) * (N-2) * (N-3) * ... * 1.

Entrada
A entrada contém um valor inteiro N (0 < N < 13).

Saída
A saída contém um valor inteiro, correspondente ao fatorial de N.

Entrada     Saída
4           24
'''
n = int(input())

fat = 1                  # Valor neutro da multiplicação
for i in range(1, n+1):  # Pode-se usar range(2, n+1), pelo motivo acima
    fat *= i

print(fat)

