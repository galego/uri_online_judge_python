'''
Ler um número inteiro N e calcular todos os seus divisores.

Entrada
O arquivo de entrada contém um valor inteiro.

Saída
Escreva todos os divisores positivos de N, um valor por linha.

Entrada     Saída
6           1
            2
            3
            6
'''
x = int(input())
for i in range(1, x+1):
    if x % i == 0:
        print(i)

