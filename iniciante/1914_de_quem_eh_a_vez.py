'''
Amarelinha provavelmente é a brincadeira em que as crianças da vila mais se
divertem, porém a mesma vem causando um bom tempo de discussão e choro nas
crianças que a praticam. A causa do transtorno é para decidir quem será o
próximo a pular, mas recentemente Quico (O gênio!) teve uma grande ideia
para solucionar o problema. Basicamente a brincadeira só poderá ser jogada
de dois em dois jogadores e para escolher o próximo jogador Quico indicou o
uso do tradicional método par ou ímpar, onde os dois jogadores informam um
número e se a soma desses números for par o jogador que escolheu PAR ganha
ou vice verso. Entretanto a utilização desse método vem deixando o Quico
louco, louco, louco... E por esse motivo ele pediu a sua ajuda! Solicitou a
você um programa que dado o nome dos jogadores, suas respectivas escolhas
PAR ou IMPAR e os números, informe quem foi o vencedor.

Entrada
A primeira linha de entrada contém um número inteiro QT (1 ≤ QT ≤ 100),
indicando a quantidade de casos de teste que vem a seguir. Cada caso de
teste contém duas linhas. Na primeira linha será informado o nome do
jogador 1 seguido de sua escolha, “PAR” ou “IMPAR” e logo após, o nome do
jogador 2 seguido de sua escolha, “PAR” ou “IMPAR”. Na segunda linha de
entrada, contém 2 números inteiros N (1 ≤ N ≤ 10⁹) e M (1 ≤ M ≤ 10⁹),
representando respectivamente os números escolhidos pelo jogador 1 e pelo
jogador 2. É garantido que a escolha (PAR ou IMPAR) do jogador 1 será
diferente da escolha (PAR ou IMPAR) do jogador 2 e que o nome dos jogares
são formados somente por letras e não ultrapassarão 100 caracteres.

Saída
Para cada caso de teste, imprima uma única linha contendo o nome do jogador
vencedor.

Entrada                     Saída
4
Quico PAR Chiquinha IMPAR   Quico
9 7
Dami PAR Marcus IMPAR       Marcus
12 3
Dayran PAR Conrado IMPAR    Conrado
3 1000000000
Popis PAR Chaves IMPAR      Chaves
2 7
'''
testes = int(input())
for i in range(testes):
    nome1, mao1, nome2, mao2 = input().split()
    n1, n2 = map(int, input().split())
    s = n1 + n2
    if s % 2 == 0:
        ganha = 'PAR'
    else:
        ganha = 'IMPAR'

    if ganha == mao1:
        print(nome1)
    else:
        print(nome2)

