'''
Leia um valor inteiro N. Este valor será a quantidade de valores que serão
lidos em seguida. Para cada valor lido, mostre uma mensagem em inglês
dizendo se este valor lido é par (EVEN), ímpar (ODD), positivo (POSITIVE)
ou negativo (NEGATIVE). No caso do valor ser igual a zero (0), embora a
descrição correta seja (EVEN NULL), pois por definição zero é par, seu
programa deverá imprimir apenas NULL.

A primeira linha da entrada contém um valor inteiro N(N < 10000) que indica
o número de casos de teste. Cada caso de teste a seguir é um valor inteiro
X (-10**7 < X <10**7).

Entrada         Saída
4               ODD NEGATIVE
-5              NULL
0               ODD POSITIVE
3               EVEN NEGATIVE
-4
'''
n = int(input())
for i in range(n):
    x = int(input())
    if x == 0:
        print('NULL')
    else:
        if x % 2 == 0:
            if x < 0:
                print('EVEN NEGATIVE')
            else:
                print('EVEN POSITIVE')
        else:
            if x < 0:
                print('ODD NEGATIVE')
            else:
                print('ODD POSITIVE')

