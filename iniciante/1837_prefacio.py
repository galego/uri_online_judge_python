'''Para aquecer você para esta competição, vamos pedir que você desenvolva
um programa que calcule o quociente e o resto da divisão de dois números
inteiros, pode ser? Lembre que o quociente e o resto da divisão de um
inteiro a por um inteiro não-nulo b são respectivamente os únicos inteiros
q e r tais que 0 ≤ r < |b| e:

a = b × q + r

Caso você não saiba, o teorema que garante a existência e a unicidade dos
inteiros q e r é conhecido como ‘Teorema da Divisão Euclidiana’ ou
‘Algoritmo da Divisão’.

Entrada
A entrada é composta por dois números inteiros a e b
(-1.000 ≤ a, b < 1.000).

Saída
Imprima o quociente q seguido pelo resto r da divisão de a por b.

Entrada     Saída
7 3         2 1
7 -3        -2 1
-7 3        -3 2
'''
# Meu código (accepted)
a, b = list(map(int, input().split()))
y = abs(b)

r = a % y     # Resto
q = a // y    # Quociente
if b < 0:
    q = -q
print(a == (b*q)+r)
print(q, r)

'''
# Solução que vi no github (provavelmente accepted)
a, b = [int(x) for x in input().split()]
for r in range(abs(b)):
    if ((a - r) % b) == 0:
        q = int((a - r)/b)
        break
print(q, r)
'''
