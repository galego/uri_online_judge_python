'''
Na matemática, um número perfeito é um número inteiro para o qual a soma de
todos os seus divisores positivos próprios (excluindo ele mesmo) é igual ao
próprio número. Por exemplo o número 6 é perfeito, pois 1+2+3 é igual a 6.
Sua tarefa é escrever um programa que imprima se um determinado número é
perfeito ou não.

Entrada
A entrada contém vários casos de teste. A primeira linha da entrada contém
um inteiro N (1 ≤ N ≤ 20), indicando o número de casos de teste da entrada.
Cada uma das N linhas seguintes contém um valor inteiro X (1 ≤ X ≤ 108), que
pode ser ou não, um número perfeito.

Saída
Para cada caso de teste de entrada, imprima a mensagem “X eh perfeito” ou
“X nao eh perfeito”, de acordo com a especificação fornecida.

Entrada         Saída
3               6 eh perfeito
6               5 nao eh perfeito
5               28 eh perfeito
28
'''
# O código funciona, mas o site não aceitou (erro 5%)
def verificar_perfeito(num):
    possiveis_divisores = num // 2 + 1   # O site não aceita assim, teria
    soma = 0                             # que percorrer todos os valores
    for w in range(possiveis_divisores, 0, -1):  # até num
        if num % w == 0:
            soma += w
    if soma == num:
        print('{} eh perfeito'.format(num))
    else:
        print('{} nao eh perfeito'.format(num))


testes = int(input())

for w in range(testes):
    num = int(input())
    verificar_perfeito(num)

