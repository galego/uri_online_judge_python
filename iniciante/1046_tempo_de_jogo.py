'''
Leia a hora inicial e a hora final de um jogo. A seguir calcule a duração do
jogo, sabendo que o mesmo pode começar em um dia e terminar em outro, tendo
uma duração mínima de 1 hora e máxima de 24 horas.

Entrada         Saída
16 2            O JOGO DUROU 10 HORA(S)
0 0             O JOGO DUROU 24 HORA(S)
2 16            O JOGO DUROU 14 HORA(S)
'''
inicio, fim = input().split()  # Horas e minutos separados por espaço
inicio = int(inicio)
fim = int(fim)

if inicio >= fim:
    total = (24 - inicio) + fim
else:
    total = fim - inicio

print('O JOGO DUROU {} HORA(S)'.format(total))

