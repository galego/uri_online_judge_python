'''
Na matemática, um Número Primo é aquele que pode ser dividido somente por
1 (um) e por ele mesmo. Por exemplo, o número 7 é primo, pois pode ser
dividido apenas pelo número 1 e pelo número 7.

Entrada
A entrada contém vários casos de teste. A primeira linha da entrada contém
um inteiro N (1 ≤ N ≤ 100), indicando o número de casos de teste da entrada.
Cada uma das N linhas seguintes contém um valor inteiro X (1 < X ≤ 107),
que pode ser ou não, um número primo.

Saída
Para cada caso de teste de entrada, imprima a mensagem “X eh primo” ou
“X nao eh primo”, de acordo com a especificação fornecida.

Entrada     Saída
3           8 nao eh primo
8           51 nao eh primo
51          7 eh primo
7
'''
testes = int(input())
for i in range(testes):
    x = int(input())
    metade = x//2+1  # O site não aceita assim. Logo, remover essa linha
    divisor = False
    for j in range(2, metade+1):  # Substituir por: range(2, x)
        if x % j == 0:
            divisor = True
    if divisor:
        print('{} nao eh primo'.format(x))
    else:
        print('{} eh primo'.format(x))

