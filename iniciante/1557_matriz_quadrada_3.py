'''
Escreva um algoritmo que leia um inteiro N (0 ≤ N ≤ 15), correspondente a
ordem de uma matriz M de inteiros, e construa a matriz de acordo com o
exemplo abaixo.

Entrada
A entrada consiste de vários inteiros, um valor por linha, correspondentes
as ordens das matrizes a serem construídas. O final da entrada é marcado
por um valor de ordem igual a zero (0).

Saída
Para cada inteiro da entrada imprima a matriz correspondente, de acordo com
o exemplo. Os valores das matrizes devem ser formatados em um campo de
tamanho T justificados à direita e separados por espaço, onde T é igual ao
número de dígitos do maior número da matriz. Após o último caractere de
cada linha da matriz não deve haver espaços em branco. Após a impressão de
cada matriz deve ser deixada uma linha em branco.

Entrada     Saída
1           1

2           1 2
            2 4

3           1  2  4
            2  4  8
            4  8  16

4           1  2  4  8
            2  4  8 16
            4  8 16 32
            8 16 32 64

5            1   2   4   8  16
             2   4   8  16  32
             4   8  16  32  64
             8  16  32  64 128
            16  32  64 128 256
0
'''
while 1:
    ordem = int(input())  # Ordem (tamanho) da matriz
    if ordem == 0:
        break
    
    matriz = []
    maior_numero = ''  # Útil para formatar o output
    
    # Criando a primeira linha da matriz, que servirá como ponto inicial
    base = []
    k = 1  # Valor inicial para criar os outros da 1ª linha
    for i in range(ordem):
        base.append(k)
        k *= 2

    # Essa cópia se faz necessária pois iremos alterar a linha base depois,
    # se não fizermos isso, as 'linhas' já adicionadas à matriz sofrerão
    # alterações posteriores.
    linha = base[:]
    matriz.append(linha)

    # Criamos as novas linhas até que a matriz tenha o tamanho certo
    while len(matriz) < ordem:
        x = base.pop(0)  # Apenas para remover o valor que já não queremos
        novo = base[-1] * 2  # Devesse dobrar o último valor
        base.append(novo)    # e adicioná-lo ao final da linha

        if len(str(novo)) > len(maior_numero):
            maior_numero = str(novo)
        linha = base[:]   # Mesma razão anterior
        matriz.append(linha)

    # Após a matriz criada, vamos exibí-la corretamente
    # criando a quantidade de máscaras necessárias para cada linha
    exibir = ('{:>%d} ' %(len(maior_numero))) * ordem
    exibir = exibir.strip()  # Remover o espaço ao fim da linha

    for linha in matriz:
        print(exibir.format(*linha))
    print()

