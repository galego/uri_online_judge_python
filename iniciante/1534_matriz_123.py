'''
Leia um valor inteiro N que é o tamanho da matriz que deve ser impressa
conforme o modelo fornecido.

Entrada
A entrada contém vários casos de teste e termina com EOF. Cada caso de teste
é composto por um único inteiro N (3 ≤ N < 70), que determina o tamanho
(linhas e colunas) de uma matriz que deve ser impressa.

Saída
Para cada N lido, apresente a saída conforme o exemplo fornecido.

Entrada     Saída
4           1332
7           3123
            3213
            2331
            1333332
            3133323
            3313233
            3332333
            3323133
            3233313
            2333331
'''
while True:
    '''    ordem = input()
    if ordem:
        ordem = int(ordem)
    else:
        break'''
    try:
        ordem=int(input())
    except EOFError:
        break
    matriz = [[i for i in range(ordem)] for j in range(ordem)]
    soma_dos_indices = ordem - 1
    for i in range(ordem):
        for j in range(ordem):
            if i + j == soma_dos_indices:
                matriz[i][j] = 2
            elif i == j:
                matriz[i][j] = 1
            else:
                matriz[i][j] = 3

    exibir = '{}' * ordem
    for linha in matriz:
        print(exibir.format(*linha))

