# Solução com funções
from decimal import Decimal

def criar_matriz():
    t = 0
    matrix = []
    for l in range(12):
        line = []
        for c in range(12):
            x = Decimal(testes[t])
            line.append(x)
            t += 1
        matrix.append(line)
    return matrix

def rotacionar_matriz(matrix):
    # 'Rotaciona' a matriz para o lado direito
    # 1 2 3 4 __    1 5 9 7
    # 5 6 7 8 __>>  2 6 2 9
    # 9 2 3 5       3 7 3 0
    # 7 9 0 2       4 8 5 2
    m = []
    for coluna in range(12):
        l = []
        for linha in range(12):
            x = matrix[linha][coluna]
            l.append(x)
        m.append(l[::-1])     # Se quiser retornar realmente rotacionado
                        # basta usar l[::-1]
                        # 7 9 5 1
                        # 9 2 6 2
                        # 0 3 7 3
                        # 2 5 8 4
    return m[::-1]

def extrair_dados(matrix):
    soma = 0
    itens = 0
    i = 1
    j = 11
    for linha in matrix:
        l = linha[i:j]
        soma = soma + sum(l)
        itens = itens + len(l)
        i += 1
        j -= 1
        if i >= j:
            break
    return soma, itens

op = input().upper()

# Criar matriz
matriz = criar_matriz()
matriz_aux = rotacionar_matriz(matriz)
soma, itens = extrair_dados(matriz_aux)

if op == 'S':
    print('{:.1f}'.format(soma))
elif op == 'M':
    media = soma/itens
    print('{:.1f}'.format(media))

