'''
I=0 J=1
I=0 J=2
I=0 J=3
I=0.2 J=1.2
I=0.2 J=2.2
I=0.2 J=3.2
.....
I=2 J=?
I=2 J=?
I=2 J=?
'''
# Esse código foi um pouco traiçoeiro
from fractions import Fraction
a = Fraction(1, 5)
i = Fraction(0, 1)

while i <= 2:
    for j in range(1, 4):
        if int(i) == float(i):
            print(f'I={int(i)} J={j+int(i)}')
        else:
            print(f'I={float(i)} J={j+float(i)}')
    i += a
