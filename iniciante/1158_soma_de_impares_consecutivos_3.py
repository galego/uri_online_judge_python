'''
Leia um valor inteiro N que é a quantidade de casos de teste que vem a
seguir. Cada caso de teste consiste de dois inteiros X e Y. Você deve
apresentar a soma de Y ímpares consecutivos a partir de X inclusive o
próprio X se ele for ímpar. Por exemplo:
para a entrada 4 5, a saída deve ser 45, que é equivalente à:
5 + 7 + 9 + 11 + 13
para a entrada 7 4, a saída deve ser 40, que é equivalente à:
7 + 9 + 11 + 13

Entrada
A primeira linha de entrada é um inteiro N que é a quantidade de casos de
teste que vem a seguir. Cada caso de teste consiste em uma linha contendo
dois inteiros X e Y.

Saída
Imprima a soma dos consecutivos números ímpares a partir do valor X.

Entrada     Saída
2           21
4 3         24
11 2
'''
testes = int(input())

for i in range(testes):
    x, y = list(map(int, input().split()))
    cont = 0
    soma = 0
    while cont < y:
        if x % 2 != 0:
            cont += 1   # Contabilizar o número ímpar encontrado
            soma += x   # Somar os números ímpares
        x += 1          # Incrementar o valor de X para achar mais ímpares
    print(soma)

