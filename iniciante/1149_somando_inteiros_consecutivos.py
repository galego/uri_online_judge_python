'''
Faça um algoritmo para ler um valor A e um valor N. Imprimir a soma de A
para cada i com os valores (0 <= i <= N-1). Enquanto N for negativo ou ZERO,
um novo N(apenas N) deve ser lido.

Entrada
A entrada contém somente valores inteiros, podendo ser positivos ou
negativos. Todos os valores estão na mesma linha.

Saída
A saída contém apenas um valor inteiro.

Entrada         Saída
3 2             7
3 -1 0 -2 2     7
'''
# Eu não entendi bem esse enunciado junto a saída que se pede, logo,
# resolvi esse exercício vendo uma resolução em C.
# Essa parte de input() na mesma linha quando o valor for inválido, foi
# traiçoeira.
# O CÓDIGO AQUI FUNCIONA. Porém, não é a solução aceita pelo site uri
# Assim sendo, segue o código na solução 2

# OBS.: Após analisar o código em C, consegui entender o que se pede.
# Segue a minha resolução com comentários

soma = 0  # Variável para acumular o somatório que se pede

a, n = list(map(int, input().split()))

while n <= 0:  # Se, e enquanto, N <= 0, continua pedindo
    n = int(input())

for i in range(n):  # Aqui o loop ocorrerá N vezes
    soma += a       # Em cada loop, somasse o valor de A
    a += 1          # Porém, o valor de A também é incrementado em 1

print(soma)

