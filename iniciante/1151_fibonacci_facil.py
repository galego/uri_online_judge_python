'''
A seguinte sequência de números 0 1 1 2 3 5 8 13 21... é conhecida como
série de Fibonacci. Nessa sequência, cada número, depois dos 2 primeiros, é
igual à soma dos 2 anteriores. Escreva um algoritmo que leia um inteiro N
(N < 46) e mostre os N primeiros números dessa série.

Entrada
O arquivo de entrada contém um valor inteiro N (0 < N < 46).

Saída
Os valores devem ser mostrados na mesma linha, separados por um espaço em
branco. Não deve haver espaço após o último valor.

Entrada     Saída
5           0 1 1 2 3
'''
n = int(input())

fib = ['0', '1']
a, b = map(int, fib)

for i in range(n-2):  # Pois os dois primeiros já estão definidos A e B
    a, b = b, a + b
    fib.append(str(b))

print(' '.join(fib))

