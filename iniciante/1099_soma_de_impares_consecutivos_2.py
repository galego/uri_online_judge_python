'''
Leia um valor inteiro N que é a quantidade de casos de teste que vem a
seguir. Cada caso de teste consiste de dois inteiros X e Y. Você deve
apresentar a soma de todos os ímpares existentes entre X e Y.

Entrada
A primeira linha de entrada é um inteiro N que é a quantidade de casos de
teste que vem a seguir. Cada caso de teste consiste em uma linha contendo
dois inteiros X e Y.

Saída
Imprima a soma de todos valores ímpares entre X e Y.

Entrada     Saída
7           0
4 5         11
13 10       5
6 4         0
3 3         0
3 5         0
3 4         12
3 8
'''
x = int(input('Número de testes: '))

for i in range(x):
    a, b = input().split()
    a = int(a)
    b = int(b)
    if a > b:
        a, b = b, a
    soma = 0
    for j in range(a+1, b):
        if j % 2 != 0:
            soma += j
    print(soma)

