'''
Dois carros (X e Y) partem em uma mesma direção. O carro X sai com
velocidade constante de 60 Km/h e o carro Y sai com velocidade constante de
90 Km/h.

Em uma hora (60 minutos) o carro Y consegue se distanciar 30 quilômetros do
carro X, ou seja, consegue se afastar um quilômetro a cada 2 minutos.

Leia a distância (em Km) e calcule quanto tempo leva (em minutos) para o
carro Y tomar essa distância do outro carro.

Entrada         Saída
110             220 minutos
30              60 minutos
7               14 minutos
'''
distancia = int(input())  # Distância em km
tempo = distancia * 2
print('%i minutos' %tempo)

