'''
Faça um programa que leia o nome de um vendedor, o seu salário fixo e o
total de vendas efetuadas por ele no mês (em dinheiro). Sabendo que este
vendedor ganha 15% de comissão sobre suas vendas efetuadas, informar o total
a receber no final do mês, com duas casas decimais.

Entrada          Saída
JOAO
500.00
1230.30          TOTAL = 684.54
'''
nome = input()  # Nome do vendedor
salario_fixo = float(input())  # Salário do vendedor
total_vendas = float(input())  # Valor do total de vendas

comissao = 0.15 * total_vendas
salario_final = salario_fixo + comissao
print(f'TOTAL = U$ {salario_final:.2f}')

