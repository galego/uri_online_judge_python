'''
Escreva um algoritmo para calcular e escrever o valor de S, sendo S dado
pela fórmula:
S = 1 + 3/2 + 5/4 + 7/8 + ... + 39/?

Entrada
Não há nenhuma entrada neste problema.

Saída
A saída contém um valor correspondente ao valor de S.
O valor deve ser impresso com dois dígitos após o ponto decimal.
'''
numerador = 3
denominador = 2
soma = 1  # Pois não posso dividir 1 por 0, logo, já o adiciono à soma

while numerador <= 39:
    soma = soma + (numerador / denominador)
    numerador += 2
    denominador *= 2

print(f'{soma:.2f}')

