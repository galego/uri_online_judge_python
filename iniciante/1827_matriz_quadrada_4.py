'''
Neste programa seu trabalho é ler um valor inteiro que será o tamanho da
matriz quadrada (largura e altura) que será preenchida da seguinte forma:
a parte externa é preenchida com 0, a parte interna é preenchida com 1, a
diagonal principal é preenchida com 2, a diagonal secundária é preenchida
com 3 e o ponto central contém o valor 4, conforme os exemplos abaixo.

Obs: o quadrado com '1' sempre começa na posição tamanho/3, tanto na
largura quanto quanto na altura. A linha e a coluna começam em zero (0).

Entrada
A entrada contém vários casos de teste e termina com EOF (fim de arquivo.
Cada caso de teste consiste de um valor inteiro ímpar N (5 ≤ N ≤ 101) que é
o tamanho da matriz.

Saída
Para cada caso de teste, imprima a matriz correspondente conforme o
exemplo abaixo. Após cada caso de teste, imprima uma linha em branco.

Entrada         Saída
5               20003
                01110
                01410
                01110
                30002

11              20000000003
                02000000030
                00200000300
                00011111000
                00011111000
                00011411000
                00011111000
                00011111000
                00300000200
                03000000020
                30000000002
'''
while 1:
    try:
        ordem = int(input())
    except EOFError:
        break

    terco = ordem // 3
    half = ordem // 2
    # Como a matriz é um 'espelho' (praticamente), vamos criar apenas
    # metade e depois espelhar
    m = [[0 for d in range(ordem)] for t in range(half)]

    i = 0           # Índice do começo da linha
    j = ordem - 1   # Índice do fim da linha
    for a in range(terco): # Só precisa percorrer 1/3 das linhas
        m[a][i] = 2     # matriz na linha A e índice I recebe 2
        m[a][j] = 3     # matriz na linha A e índice J recebe 3
        i += 1
        j -= 1

    for l in range(len(m)):   # Agora percorremos todas as linhas já
        if 2 in m[l]:         # criadas. Se elas tiverem os valores 2 e 3,
            dois = m[l].index(2) # não mexemos, mas pegamos os índices
            tres = m[l].index(3)
        else:
            # Quando a linha for só de zeros, populamos com 1, do índice
            # onde tinha o dois + 1 até o índice onde tinha o tres. Pois é
            # a parte mais interna da matriz
            for d in range(dois+1,tres):
                m[l][d] = 1

    # Ainda faltará o miolo, onde deve ter o número 4
    meio = m[-1][:]  # Copiamos a última linha já criada, sem a referência
    meio[half] = 4   # e o elemento do índice 'half' recebe o valor 4
    m.append(meio)   # Adicionamos essa nova linha à matriz

    for l in m[-2::-1]:   # Percorremos a matriz inversamente, sem a última
        m.append(l[::-1]) # linha. Então adicionamos cada linha (inversa)
                          # à matriz.

    for l in m:
        for k in l:
            print(k, end='')
        print()
    print()

