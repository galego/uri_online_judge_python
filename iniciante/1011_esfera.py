'''
Faça um programa que calcule e mostre o volume de uma esfera sendo fornecido
o valor de seu raio (R). A fórmula para calcular o volume é: (4/3)*pi*R3.
Considere (atribua) para pi o valor 3.14159.

Dica: Ao utilizar a fórmula, procure usar (4/3.0) ou (4.0/3), pois algumas
linguagens (dentre elas o C++), assumem que o resultado da divisão entre
dois inteiros é outro inteiro.

Entrada           Saída
3                 VOLUME = 113.097
'''
import math

# Valor do raio
r = float(input())

# Cálculo do volume da esfera
volume = (4/3) * math.pi * r**3
print(f'VOLUME = {volume}')

