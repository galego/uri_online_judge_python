'''
Leia 2 valores inteiros X e Y. A seguir, calcule e mostre a soma dos
números impares entre eles.

Entrada         Saída
6               5
-5

15              13
12

12              0
12
'''

x = int(input('X: '))
y = int(input('Y: '))

if y > x:
    x, y = y, x

c = 0
for i in range(x-1, y, -1):
    if i % 2 != 0:
        c += i
print(c)

