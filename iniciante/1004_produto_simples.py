'''
Leia dois valores inteiros. A seguir, calcule o produto entre estes dois
valores e atribua esta operação à variável PROD. A seguir mostre a variável
PROD com mensagem correspondente.

Entrada      Saída
3
9            PROD = 27
'''

# Input dos valores A e B
a = int(input())
b = int(input())
prod = a * b
print('PROD = %i' %prod)

