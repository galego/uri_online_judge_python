'''
Em um país imaginário denominado Lisarb, todos os habitantes ficam felizes
em pagar seus impostos, pois sabem que nele não existem políticos corruptos
e os recursos arrecadados são utilizados em benefício da população, sem
qualquer desvio. A moeda deste país é o Rombus, cujo símbolo é o R$.

Leia um valor com duas casas decimais, equivalente ao salário de uma pessoa
de Lisarb. Em seguida, calcule e mostre o valor que esta pessoa deve pagar
de Imposto de Renda, segundo a tabela abaixo.

    Renda               IR
de 0.00 a 2000.00     isento
de 2000.01 a 3000.00    8%
de 3000.01 a 4500.00    18%
acima de 4500.00        28%

Lembre que, se o salário for R$ 3002.00, a taxa que incide é de 8% apenas
sobre R$ 1000.00, pois a faixa de salário que fica de R$ 0.00 até R$ 2000.00
é isenta de Imposto de Renda. No exemplo fornecido (abaixo), a taxa é de 8%
sobre R$ 1000.00 + 18% sobre R$ 2.00, o que resulta em R$ 80.36 no total.
O valor deve ser impresso com duas casas decimais.

Entrada             Saída
3002.00             R$ 80.36
1701.02             Isento
4520.00             R$ 355.60
'''
salario = float(input())  # Salário
valor = salario

if salario <= 2000:
    print('Isento')
else:
    ir = 0
    if salario > 4500:
        resto = salario - 4500
        ir = ir + 0.28 * resto
        salario -= resto
    if salario > 3000:
        resto = salario - 3000
        ir = ir + 0.18 * resto
        salario -= resto
    if salario > 2000:
        resto = salario - 2000
        ir = ir + 0.08 * resto

    print('R$ {:.2f}'.format(ir))

