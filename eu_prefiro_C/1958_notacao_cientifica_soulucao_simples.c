/*
Vi essa solução em um código python na net: github > mcavalca
# Apenas mudei algumas coisas
from decimal import Decimal

n = Decimal(input())
if n < 0:
    print('{:.4E}'.format(n))
else:
    print('+{:.4E}'.format(n))
*/