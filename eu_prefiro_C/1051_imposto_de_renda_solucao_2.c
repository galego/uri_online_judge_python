/*
l = float(input())  # Salário

# faixa salarial e sua respectiva taxa de imposto
impostos = [(4500, 0.28), (3000, 0.18), (2000, 0.08)]

if sal <= 2000:
    print('Isento')
else:
    ir = 0
    for s, imp in impostos:
        if sal > s:
            resto = sal - s
            ir = ir + imp * resto
            sal -= resto
    print('R$ {:.2f}'.format(ir))
*/