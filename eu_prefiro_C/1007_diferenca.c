/*
Leia quatro valores inteiros A, B, C e D. A seguir, calcule e mostre a
diferença do produto de A e B pelo produto de C e D segundo a fórmula:
DIFERENCA = (A * B - C * D).

Entrada        Saída
5
6
7
8              DIFERENÇA = -26
*/
#include <stdio.h>

int main() {
    int a, b, c, d, dif;

    scanf("%d %d %d %d", &a, &b, &c, &d);
    dif = a * b - c * d;
    printf("DIFERENCA = %d", dif);

    return 0;
}
