/*
A fórmula para calcular a área de uma circunferência é: area = π . raio2.
Considerando para este problema que π = 3.14159:

- Efetue o cálculo da área, elevando o valor de raio ao quadrado e
multiplicando por π.
Saída com 4 casas decimais.

Entrada       Saída
2.00          A=12.5664
*/
#include <stdio.h>
// a lib math.h só foi reconhecida pelo g++ e não pelo gcc
// Descobri, tem que usar a tag -lm junto ao gcc -->> tá no .vimrc
#include <math.h>

int main() {
    // Poderia ter usado float também
    // No site URI, só foi aceito usando double
    double raio, area;
    double PI = 3.14159;

    // lf - long float - double
    //  f -      float - float
    scanf("%lf", &raio);
    area = PI * pow(raio, 2);

    printf("A=%.4f\n", area);
    return 0;
}
