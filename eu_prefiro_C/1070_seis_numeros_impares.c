/*

Leia um valor inteiro X. Em seguida apresente os 6 valores ímpares
consecutivos a partir de X, um valor por linha, inclusive o X ser for o
caso.

Entrada         Saída
8               9
                11
                13
                15
                17
                19
*/