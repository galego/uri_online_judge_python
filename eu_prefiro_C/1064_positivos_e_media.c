/*

Leia 6 valores. Em seguida, mostre quantos destes valores digitados foram
positivos. Na próxima linha, deve-se mostrar a média de todos os valores
positivos digitados, com um dígito após o ponto decimal.

Entrada         Saída
7               4 valores positivos
-5              7.4
6
-3.4
4.6
12
*/