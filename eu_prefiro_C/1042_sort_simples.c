/*

Leia 3 valores inteiros e ordene-os em ordem crescente. No final, mostre os
valores em ordem crescente, uma linha em branco e em seguida, os valores na
sequência como foram lidos.

Entrada             Saída
7 21 -14            -14
                    7
                    21

                    7
                    21
                    -14

-14 21 7            -14
                    7
                    21

                    -14
                    21
                    7
*/