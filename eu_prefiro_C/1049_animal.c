/*

Neste problema, você deverá ler 3 palavras que definem o tipo de animal
possível segundo o esquema abaixo, da esquerda para a direita.  Em seguida
conclua qual dos animais seguintes foi escolhido, através das três palavras
fornecidas.

                / carnívoro - águia
          / ave - onívoro   - pomba
vertebrado
          \ mamífero - onívoro   - homem
                     \ herbívoro - vaca
###########################################
                     / hematófago - pulga
            / inseto - herbívoro - lagarta
invertebrado
            \ anelídeo - hematófago - sanguessuga
                       \ onívoro    - minhoca

A entrada contém 3 palavras, uma em cada linha, necessárias para identificar
o animal segundo a figura acima, com todas as letras minúsculas.

Entrada         Saída
vertebrado      homem
mamifero
onivoro

vertebrado      aguia
ave
carnivoro

invertebrado    minhoca
anelideo
onivoro
*/