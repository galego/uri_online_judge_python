/*

Leia 3 valores de ponto flutuante A, B e C e ordene-os em ordem decrescente,
de modo que o lado A representa o maior dos 3 lados. A seguir, determine o
tipo de triângulo que estes três lados formam, com base nos seguintes casos,
sempre escrevendo uma mensagem adequada:
se A ≥ B+C, apresente a mensagem: NAO FORMA TRIANGULO
se A**2 = B**2 + C**2, apresente a mensagem: TRIANGULO RETANGULO
se A**2 > B**2 + C**2, apresente a mensagem: TRIANGULO OBTUSANGULO
se A**2 < B**2 + C**2, apresente a mensagem: TRIANGULO ACUTANGULO
se os três lados forem iguais, apresente a mensagem: TRIANGULO EQUILATERO
se apenas dois dos lados forem iguais, apresente a mensagem:
TRIANGULO ISOSCELES

Entrada         Saída
7.0 5.0 7.0     TRIANGULO ACUTANGULO
                TRIANGULO ISOSCELES

6.0 6.0 10.0    TRIANGULO OBTUSANGULO
                TRIGANGULO ISOSCELES

6.0 6.0 6.0     TRIANGULO ACUTANGULO
                TRIANGULO EQUILATERO

5.0 7.0 2.0     NAO FORMA TRIANGULO

6.0 8.0 10.0    TRIANGULO RETANGULO
*/