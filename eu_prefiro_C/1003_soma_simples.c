/*
Leia dois valores inteiros, no caso para variáveis A e B. A seguir, calcule
a soma entre elas e atribua à variável SOMA. A seguir escrever o valor
desta variável.

Entrada       Saída
30
10            SOMA = 40
*/
#include <stdio.h>

int main() {
    int a, b, s;

    scanf("%i %i", &a, &b);
    s = a + b;
    printf("SOMA = %d\n", s);

    return 0;
}
