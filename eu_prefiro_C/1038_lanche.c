/*

Com base na tabela abaixo, escreva um programa que leia o código de um item
e a quantidade deste item. A seguir, calcule e mostre o valor da conta a
pagar.

CODIGO    ESPECIFICAÇÃO    PREÇO
1         Cachorro Quente  R$ 4.00
2         X-Salada         R$ 4.50
3         X-Bacon          R$ 5.00
4         Torrada simples  R$ 2.00
5         Refrigerante     R$ 1.50


O arquivo de entrada contém dois valores inteiros correspondentes ao código
e à quantidade de um item conforme tabela acima.

Entrada         Saída
3 2             Total: R$ 10.00
4 3             Total: R$ 6.00
2 3             Total: R$ 13.50
*/