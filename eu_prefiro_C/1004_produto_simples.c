/*
Leia dois valores inteiros. A seguir, calcule o produto entre estes dois
valores e atribua esta operação à variável PROD. A seguir mostre a variável
PROD com mensagem correspondente.

Entrada      Saída
3
9            PROD = 27
*/
#include <stdio.h>

int main() {
    int a, b, p;

    scanf("%d %d", &a, &b);
    p = a * b;
    printf("PROD = %d\n", p);

    return 0;
}
