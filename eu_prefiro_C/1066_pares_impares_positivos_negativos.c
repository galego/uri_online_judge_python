/*

Leia 5 valores Inteiros. A seguir mostre quantos valores digitados foram
pares, quantos valores digitados foram ímpares, quantos valores digitados
foram positivos e quantos valores digitados foram negativos.

Entrada         Saída
-5              3 valor(es) par(es)
0               2 valor(es) impar(es)
-3              1 valor(es) positivo(s)
-4              3 valor(es) negativo(s)
12
*/