/*

Leia um valor inteiro N. Apresente todos os números entre 1 e 10000 que
divididos por N dão resto igual a 2.

A entrada contém um valor inteiro N (N < 10000).

Entrada         Saída
13              2
                15
                28
                41
                ...
*/