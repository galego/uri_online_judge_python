/*

Dabriel é um menino fissurado por matemática, ele acaba de aprender em sua escola
operações sobre conjuntos. Após passar a tarde toda brincando com alguns conjuntos que
ele possui, chega a hora de resolver as lições de casa, porém ele já está muito
cansado e com medo de que possa cometer alguns erros, solicitou sua ajuda.
Dabriel deseja um programa de computador que dado N conjuntos e os elementos de cada
conjunto, ele possa realizar algumas operações, são elas:
1 X Y: Retorna a quantidade de elementos distintos da intersecção entre o conjunto X
com o Y.
2 X Y: Retorna a quantidade de elementos distintos da união entre o conjunto X com o
Y.

Entrada
A entrada é composta por diversas instâncias. A primeira linha da entrada contém um
inteiro T indicando o número de instâncias.

Cada instância inicia com um inteiro N (1 ≤ N ≤ 10⁴), representando a quantidade de
conjuntos que Dabriel possui. As próximas N linhas começam com um inteiro Mi
(1 ≤ Mi ≤ 60), que indica o total de elementos que o conjunto i possui, segue então
Mi inteiros Xij (1 ≤ Xij ≤ 60), que representam o valor de cada elemento.
Na próxima linha contém um inteiro Q (1 ≤ Q ≤ 10⁶), representando quantas operações
Dabriel deseja realizar. Nas próximas Q linhas terá a descrição de uma operação.

Saída
Para cada operação seu programa deverá imprimir a quantidade de elementos, conforme
explicado na descrição.

Input
1
4
1 1
2 1 5
3 2 4 6
4 1 3 5 7
5
1 1 2
1 1 4
2 1 4
2 3 4
1 2 4
                

Output
1
1
4
7
2
*/