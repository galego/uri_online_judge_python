/*
Escreva um programa que leia o número de um funcionário, seu número de horas
trabalhadas, o valor que recebe por hora e calcula o salário desse
funcionário. A seguir, mostre o número e o salário do funcionário, com duas
casas decimais.

Entrada        Saída
25
100            NUMBER = 25
5.50           SALARY = U$ 550.00
*/
#include <stdio.h>

int main() {
    int num, horas;
    float valor, salario;

    scanf("%d %d %f", &num, &horas, &valor);
    salario = valor * horas;
    printf("NUMBER = %d\n", num);
    printf("SALARY = U$ %.2f", salario);

    return 0;
}
