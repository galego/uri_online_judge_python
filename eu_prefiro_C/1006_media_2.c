/*
Leia 3 valores, no caso, variáveis A, B e C, que são as três notas de um
aluno. A seguir, calcule a média do aluno, sabendo que a nota A tem peso 2,
a nota B tem peso 3 e a nota C tem peso 5. Considere que cada nota pode ir
de 0 até 10.0, sempre com uma casa decimal.

Entrada      Saída
5.0
6.0
7.0          MEDIA = 6.3
*/
#include <stdio.h>

int main() {
    float a, b, c, m;

    scanf("%f %f %f", &a, &b, &c);
    m = (a + b + c) / 3;
    printf("MEDIA = %.1f\n", m);

    return 0;
}
